/* global angular */

/**
 * Graph Total Size of DataBase Component
 *
 * @requires GraphBarDbComp
 * @requires Collection
 * @requires GraphTotalSizeOfDbData
 */
app.factory('GraphTotalSizeOfDbComp', ['GraphBarDbComp', 'Collection', 'GraphTotalSizeOfDbCollection', function (GraphBarDbComp, Collection, GraphTotalSizeOfDbCollection) {
	var GraphTotalSizeOfDbComp = function () {
		GraphBarDbComp.call(this);

		angular.merge(this.options, {
			stackSeries: true
		});

	};

	angular.extend(GraphTotalSizeOfDbComp.prototype, GraphBarDbComp.prototype, {
		updateReportData: function (data) {
			var graphLines = new GraphTotalSizeOfDbCollection(data);

			this.options.axes.xaxis.ticks = graphLines.ticks;

			return graphLines.data();
		}
	});

	return GraphTotalSizeOfDbComp;
}]);
