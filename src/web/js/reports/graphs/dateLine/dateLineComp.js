/* global angular */

/**
 * Date Line component
 *
 * @requires BaseGraphComp
 * Dependent from DateLineData Graph
 */
app.factory( 'DateLineComp', [ 'BaseGraphComp', function (BaseGraphComp) {
	var me;

	function convertData( data ) {
		const struct = data.struct;
		const result = [];

		result.struct = struct;

		const dates = data.map(function(x) {
			return new Date(x[0][struct.xvalues[0]]);
		})

		struct.yvalues.forEach( function(key) {
			var line = [];

			for ( var i = 0; i < data.length; i++ ) {
				line.push( [dates[i], data[i][1][key]] );
			}

			result.push(line);
		})

		return result;
	}

	function mergeOptions( ctx, data ) {
		if ( !ctx || !ctx.options ) {
			return;
		}

		if ( !data || !data.struct || !data.struct.options ) {
			return;
		}

		angular.merge( ctx.options, data.struct.options );
	}

	var DateLineComp = function () {
		BaseGraphComp.call( this );

		me = this;

		const opts = {
			'axes': {
				'xaxis': {
					'renderer': $.jqplot.DateAxisRenderer
				}
			}
		};

		angular.merge( this.options, opts );
	}

	angular.extend(DateLineComp.prototype, BaseGraphComp.prototype, {
		updateReportData: function ( data ) {
			mergeOptions( me, data );

			return convertData( data );
		}
	});

	return DateLineComp;
}]);
