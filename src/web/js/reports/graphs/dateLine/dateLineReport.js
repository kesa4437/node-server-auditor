/* global app */

/**
 * Date Line Directive
 *
 * @requires baseGraphLink
 * @requires DateLineComp
 */
app.directive('dateLineReport', ['baseGraphLink', 'DateLineComp', function (link, DateLineComp) {
	return {
		restrict: 'EA',
		templateUrl: 'directives/report/graphs/default-graph.html',

		scope: {
			reportItem: '=dateLineReport'
		},

		link: function ( $scope, element, attrs ) {
			link( $scope, element, attrs, DateLineComp );
		}
	}
}]);
