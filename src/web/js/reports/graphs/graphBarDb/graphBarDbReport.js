/* global app */

/**
 * Graph Bar DataBase Directive
 *
 * @requires baseGraphLink
 * @requires GraphBarDbComp
 */
app.directive('graphBarDbReport', ['baseGraphLink', 'GraphBarDbComp', function (link, GraphBarDbComp) {
	return {
		restrict: 'EA',
		templateUrl: 'directives/report/graphs/default-graph.html',
		scope: {
			reportItem: '=graphBarDbReport'
		},
		link: function ($scope, element, attrs) {
			link($scope, element, attrs, GraphBarDbComp);
		}
	};
}]);
