/* global app */

/**
 * Two XY Lines Directive
 *
 * @requires baseGraphLink
 * @requires TwoXYLinesComp
 */
app.directive('twoXYLinesReport', ['baseGraphLink', 'TwoXYLinesComp', function (link, TwoXYLinesComp) {
	return {
		restrict: 'EA',
		templateUrl: 'directives/report/graphs/default-graph.html',
		scope: {
			reportItem: '=twoXYLinesReport'
		},

		link: function ($scope, element, attrs) {
			link($scope, element, attrs, TwoXYLinesComp);
		}
	};
}]);
