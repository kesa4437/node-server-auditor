/* global app */

/**
 * Graph Bar Report Directive
 *
 * @requires baseGraphLink
 * @requires GraphBarComp
 */
app.directive('graphBarReport', ['baseGraphLink', 'GraphBarComp', function (link, GraphBarComp) {
	return {
		restrict: 'EA',
		templateUrl: 'directives/report/graphs/default-graph.html',
		scope: {
			reportItem: '=graphBarReport'
		},
		link: function ($scope, element, attrs) {
			link($scope, element, attrs, GraphBarComp);
		}
	};
}]);
