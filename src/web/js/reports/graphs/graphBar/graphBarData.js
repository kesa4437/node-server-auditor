/* global angular, app */

/**
 * Graph Bar Data Model
 *
 * @requires Data
 */
app.factory('GraphBarData', ['Data', function (Data) {
	var GraphBarData = function (params) {
		this.row = [params.Name, params.Value];
	};

	angular.extend(GraphBarData.prototype, Data.prototype, {
		data: function () {
			return this.row;
		}
	});

	return GraphBarData;
}]);
