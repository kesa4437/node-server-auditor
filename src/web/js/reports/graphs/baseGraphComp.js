/* global angular, app */

/**
 * Base Graph Component
 */
app.factory('BaseGraphComp', function () {
	var BaseGraphComp = function () {
		this.options = angular.copy(this._defaults);
	};

	angular.extend(BaseGraphComp.prototype, {
		/**
		 * Abstract function for updating report data
		 * @param {type} value
		 * @returns {undefined}
		 */
		updateReportData: function (value) {
		},

		_defaults: {
			series: [ {
					lineWidth: 2,
					markerOptions: {
						style: 'square'
					}
				}
			],
			seriesDefaults: {
				rendererOptions: {
					smooth: true
				}
			},
			axes: {
				xaxis: {
					autoscale: true
				},
				yaxis: {
					autoscale: true
				}
			}
		}
	});

	return BaseGraphComp;
});