/* global angular */

/**
 * Date Line Plain component
 *
 * @requires DateLineComp
 * Dependent from DateLine Graph
 */
app.factory('DateLinePlainComp', ['DateLineComp', function (DateLineComp) {
	var DateLinePlainComp = function () {
		DateLineComp.call(this);

		angular.merge(this.options, {
			series: [ {
					markerOptions: {
						show:      false,
						style:     'square',
						lineWidth: 2
					}
				}
			]
		});
	};

	angular.extend(DateLinePlainComp.prototype, DateLineComp.prototype, {
	});

	return DateLinePlainComp;
}]);
