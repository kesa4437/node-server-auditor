/* global angular, app */

/**
 * Base Graph Report Link function
 *
 * It sets behavior of graph directives
 */
app.factory('baseGraphLink', function () {
	return function ($scope, element, attrs, GraphClass) {
		var graphLine = new GraphClass();

		$scope.preparedData = graphLine.updateReportData($scope.reportItem);

		$scope.$watch('reportItem', function (newValue, oldValue) {
			if (newValue !== oldValue) {
				$scope.preparedData = graphLine.updateReportData(newValue);
			}
		});

		$scope.chartOpts = graphLine.options;
	};
});