(function (app) {
	app.controller('schedCreateCtrl', ['$scope', '$query', '$uibModalInstance', 'record', function ($scope, $query, $uibModalInstance, record) {
		var getTypeByModuleName;

		record = record || {};

		$scope.modules    = [];
		$scope.servers    = [];
		$scope.reports    = [];
		$scope.grupped    = [];
		$scope.moduleType = null;
		$scope.connName   = null;
		$scope.connType   = null;
		$scope.repName    = null;
		$scope.groupName  = null;
		$scope.schedule   = record.schedule || '';
		$scope.isEnabled  = record.isEnabled || false;
		$scope.forGroup   = false;

		$scope.close = function () {
			$uibModalInstance.dismiss('close');
		};

		$scope.submit = function () {
			var str_schedule = getStrSchedule();
			var connName     = ($scope.forGroup) ? 'grp' : $scope.connName;
			var data         = {
				moduleType: $scope.moduleType,
				connName:   connName,
				connType:   $scope.connType,
				repName:    $scope.repName,
				schedule:   str_schedule,
				isEnabled:  $scope.isEnabled,
				groupName:  $scope.groupName,
				forGroup:   $scope.forGroup
			};

			if ( record.id ) {
				$query('schedule/' + record.id, data, 'PUT' ).then(closeWithResult, function (res) {
					$scope.mess = res.statusText;
				});
			}
			else {
				$query('schedule', data, 'POST' ).then(closeWithResult, function (res) {
					$scope.mess = res.statusText;
				});
			}

			function closeWithResult (result) {
				var response = null;

				if ( record.id ) {
					$.extend(record, data);
					$uibModalInstance.close();
				}
				else {
					response = result.response[0];
					$uibModalInstance.close(response);
				}

			};
		};

		getStrSchedule = function() {
			return ($scope.schedule === '') ? '* * * * * 1' : $scope.schedule;
		}

		getTypeByModuleName = function (name) {
			for ( var i = 0; i < $scope.modules.length; i++ ) {
				if ( $scope.modules[i].name === name ) {
					return $scope.modules[i].type;
				}
			}
		};

		replaceStructReports = function() {
			var nameModule;

			$scope.modules.forEach(function (item) {
				nameModule = item.name;

				item.reports.forEach(function (report) {
					$scope.reports.push( { module: nameModule, name: report } );
				});
			});
		}

		$query('config/reports').then(function (data) {
			var res = data.response;

			$scope.modules = res.modules;
			$scope.servers = res.servers;
			$scope.grupped = res.grupped;

			replaceStructReports();

			$scope.moduleType = record.moduleType || $scope.modules[0].type;
			$scope.connName   = record.connName   || $scope.modules[0].name;
			$scope.connType   = record.connType   || $scope.grupped[0].t_connection.serverName;
			$scope.repName    = record.repName    || $scope.modules[0].reports[0];
			$scope.groupName  = record.groupName  || $scope.grupped[0].t_group.group_name;
			$scope.forGroup   = record.forGroup   || false;

		}, function () {
		});

		$scope.changeServerType = function( dbType ) {
			var i = 0;

			$scope.connName  = ($scope.grupped[0].t_connection.conn_type == dbType) ? $scope.grupped[0].t_connection.serverName : null;
			$scope.groupName = $scope.grupped[0].t_group.group_name;
		}
	}]);
})(app);
