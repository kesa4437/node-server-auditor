(function (app) {
	app.factory('$query', ['$http', '$q', 'sessionId', function ($http, $q, sessionId) {
		return function (query, data, method) {
			var opts;
			var deferred = $q.defer();

			data   = data || {};
			method = method || 'GET';
			query  = '/api/v1/' + query;

			opts = {
				method:  method,
				url:     query,
				headers: {
					'X-Session-ID': sessionId
				}
			};

			opts[ method === 'GET' ? 'params' : 'data' ] = data;

			$http(opts).then(function (data) {
				if (data.data.error) {
					deferred.reject( {
						status:     500,
						statusText: data.data.response,
						data:       data.data
					} );
				}
				else {
					deferred.resolve(data.data);
				}
			}, deferred.reject);

			return deferred.promise;
		};
	}]);
})(app);
