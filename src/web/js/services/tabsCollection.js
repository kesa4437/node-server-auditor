/* global app, angular */

app.service('tabsCollectionClass', [function () {
	var TabsCollection = function (mask) {
		this.mask  = mask || /(.*)/;
		this.items = [];
	};

	angular.extend(TabsCollection.prototype, {
		add: function(item) {
			var obj = {
				name:     item.name,
				active:   undefined,
				disabled: !!item.disabled
			};

			this.items.push(obj);
			return obj;
		},

		clear: function() {
			this.items.length = 0;
		},

		setActiveByRoute: function(route) {
			var name;

			name = this.mask.exec(route);

			if ( name !== null ) {
				name = name[1];
			}
			else {
				return;
			}

			for ( var key in this.items ) {
				if ( this.items.hasOwnProperty(key) && this.items[key].name === name ) {
					this.items[key].active = true;
				}
			}
		},

		getActive: function () {
			for ( var key in this.items ) {
				if ( this.items.hasOwnProperty(key) && this.items[key].active === true ) {
					return this.items[key];
				}
			}
		}
	});

	return TabsCollection;
}]);
