/* global angular, app */

app.factory('Collection', ['Data', function (Data) {
	var Collection = function (Class, arr) {
		this.container = arr.map(function (item) {
			return new Class(item);
		});
	};

	angular.extend(Collection.prototype, Data.prototype, {
		data: function () {
			return this.container.map(function(item) {
				return item.data();
			});
		}
	});

	return Collection;
}]);
