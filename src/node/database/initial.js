'use strict';

( function(module) {
	var Q                 = require('q');
	var _                 = require('lodash');
	var requireDir        = require('require-dir');
	var path              = require('path');
	var Promise           = require('promise');
	var initials          = require('configuration').initials;
	var db                = require('src/node/database')();
	var promiseWhile      = require('src/node/com/report/snapShot/promiseWhile');
	var logger            = require('src/node/log');
	var sqliteInitScripts = require('./sql_initializations')
	var appDir            = path.dirname(require.main.filename);
	var modulesPath       = path.join(appDir, 'data');
	var newconfig         = {};
	var reverseSupported  = {};
	var bulkData          = [];
	var serverConfig      = require('src/node/com/report/serverConfiguration');
	var date              = new Date();

	function init() {
		return db.sync()
			.then(sqliteInitScripts)
			.then( function() {
				db.models.t_connection_type.findAndCountAll().then( function(result) {
					for ( var key in initials.supported ) {
						reverseSupported[initials.supported[key]] = key;
					}

					if ( result.count == 0 ) {
						for ( var key in initials.supported ) {
							bulkData.push( {
								id_connection_type:   key,
								connection_type_name: initials.supported[key]
							} );
						}

						return db.models.t_connection_type.bulkCreate(bulkData);
					}
					else {
						return Q.resolve( reverseSupported );
					}
				}).then( function() {
					// let's fill the modules
					var reports  = requireDir(modulesPath);
					var bulkData = [];

					for ( var key in reports ) {
						var listReports = [];

						for ( var reportName in reports[key].reports ) {
							reports[key].reports[reportName].forEach(function (reportPath) {
								var report = require(path.join(modulesPath, reportPath));

								listReports.push( {
									rep:     report,
									relPath: reportPath
								} );
							} );
						}

						bulkData.push( {
							module_name:        key,
							id_connection_type: reverseSupported[ reports[key].type ],
							reports:            listReports
						} );
					}

					return newconfig.saveModules( bulkData );
				}).then( function() {
					newconfig.connectionType = initials.supported;
					newconfig.reverseType    = reverseSupported;

					return Q.resolve( reverseSupported );
				}).then( function( rev ) {
					var serversData = serverConfig.getServersData();
					var servers     = serverConfig.getAvailable();
					var aGroupData  = [];
					var defer       = Q.defer();

					// 1. set no group data
					aGroupData.push( {
						id:         1,
						group_name: 'No Group',
						is_active:  true,
					} );

					// find all connections and save groups
					for ( var i = 0; i < servers.length; i++ ) {
						if ( servers[i] in serversData ) {
							// check for unique group
							if ( 'groups' in serversData[servers[i]] ) {
								for ( var j = 0; j < serversData[servers[i]].groups.length; j++ ) {
									if ( findInArrayObjects( aGroupData, 'group_name', serversData[servers[i]].groups[j] ) == -1 ) {
										aGroupData.push( {
											group_name: _.clone( serversData[servers[i]].groups[j] ),
											is_active:  true
										} );
									}
								}
							}
						}
					}
					return db.models.t_group.findAndCountAll().then( function( result ) {
						if ( result && 'count' in result && result.count > 0 ) {
							return newconfig.updateGroups( aGroupData );
						}
						else {
							return db.models.t_group.bulkCreate( aGroupData );
						}
					} );
				}).then( function() {
					var servers   = serverConfig.getAvailable();
					var aConnData = [];
					var aProm     = [];
					var defer     = Q.defer();
					var srvData   = null;
					var type      = '';

					for ( var i = 0; i < servers.length; i++ ) {
						type    = '';
						srvData = serverConfig.getServersData( servers[i] );

						if ( srvData != null ) {
							type = srvData.type;
						}

						aConnData.push( {
							serverName: servers[i],
							conn_type:  type
						} );
					}
					return db.transaction( function( t ) {
						for ( var i = 0; i < aConnData.length; i++ ) {
							aProm.push( new Promise( function( resolve, reject ) {
								var rConn = _.clone( aConnData[i] );

								return db.models.t_connection.findOrCreate( {
									where: {
										serverName: rConn.serverName,
										conn_type:  rConn.conn_type
									},
									defaults: {
										serverName: rConn.serverName,
										conn_type:  rConn.conn_type
									},
									transaction: t
								} )
								.spread( function( query, created ) {
									resolve( query.dataValues );

									return defer.resolve();
								} );
							} ) );
						}
						return defer.promise;
					} ).then( function() {
						if ( aProm.length > 0 ) {
							return Promise.all( aProm ).then( function( aDataValues ) {
								return Q.resolve( aDataValues );
							}
							,function( err ) {
								console.log( err );
								return Q.reject( err )
							} );
						}
					} );
				})
				.then( function( aConnData ) {
					return newconfig.saveGroupConnections( aConnData );
				} );
			});
	};

	newconfig.saveModules = function( bulkData ) {
		return db.models.t_module.update( {
				is_active: 0
			}, {
				where: ['1']
			}
		).then( function(result) {
			return db.models.t_query.update( {
					is_active: 0
				}, {
					where: ['1']
				}
			);
		}).then( function() {
			var index = 0;

			return promiseWhile( function() {
				return index < bulkData.length
			}, function() {
				var defer = Q.defer();

				db.models.t_module.findOrCreate( {
					where: {
						module_name:        bulkData[index].module_name,
						id_connection_type: bulkData[index].id_connection_type
					}
				}).spread( function( module, created ) {
					var innerD = Q.resolve();

					if ( !created ) {
						module.is_active = true;

						innerD = module.save();
					}

					return innerD.then( function() {
						return newconfig.saveQuery(
							module.id_module,
							initials.supported[module.id_connection_type],
							bulkData[index].reports
						).then(function () {
							index ++;

							defer.resolve();
						});
					} );
				});

				return defer.promise;
			});
		}).finally( function() {
			return Q.resolve();
		});
	};

	newconfig.saveQuery = function( id_module, connectionType, reports ) {
		var index2 = 0;
	
		return promiseWhile( function() {
			return index2 < reports.length
		}, function() {
			var defer2   = Q.defer();
			var relpath  = reports[index2].relPath.split( '/' );
			var fileName = '';
			var aProm    = [];

			return db.transaction( function( t ) {
				for ( var i = 0; i < reports[index2].rep.extract.length; i++ ) {
					for ( var j = 0; j < reports[index2].rep.extract[i].file.length; j++ ) {
						aProm.push( new Promise( function( resolve, reject ) {
							var inx     = _.clone( index2 );
							var file    = _.clone( reports[inx].rep.extract[i].file[j] );
							var repName = _.clone( reports[inx].rep.name );

							return db.models.t_report.findOrCreate( {
								where: {
									report_name:    _.clone( repName ),
									id_module:      id_module
								},
								transaction: t
							}).spread( function( report, created ) {
								db.models.t_query.findOrCreate( {
									where: {
										query_location: path.join( reports[index2].relPath, _.clone( file.file )),
										id_report:    report.id_report,
									},
									transaction: t
								}).spread( function( query, created ) {
									var innerD2 = Q.resolve();
	
									if ( !created ) {
										query.is_active = true;
	
										innerD2 = query.save();
	
										resolve();
	
										return defer2.resolve();
									}
									else {
										resolve();
	
										return defer2.resolve();
									}
								});
							} );
						} ) );
					}
				}
				return defer2.promise;
			} )
			.then( function() {
				if ( aProm.length ) {
					relpath.pop();

					index2++;

					return Promise.all( aProm ).then( function( result ) {
						return defer2.promise;
					} );
				}
			}
			,function( err ) {
				logger.error( err );

				return defer2.reject();
			} );

			relpath.pop();

			return defer2.promise;
		} );
	};

	/*
	 * function to update groups
	 *
	 */
	newconfig.updateGroups = function( aGroupData ) {
		var aProm  = [];
		var defer2 = Q.defer();

		return db.transaction( function( t ) {
			for ( var i = 0; i < aGroupData.length; i++ ) {
				aProm.push( new Promise( function( resolve, reject ) {
					var rData = _.clone( aGroupData[i] );

					return db.models.t_group.findOrCreate( {
						where: {
							group_name: rData.group_name
						},
						defaults: {
							group_name: rData.group_name,
							is_active:  true
						},
						transaction: t
					} ).spread( function( query, created ) {
						var ret = Q.resolve();

						if ( !created ) {
							query.is_active = true;

							ret = query.save();

							resolve();

							return defer2.resolve();
						}
						else {
							resolve();

							return defer2.resolve();
						}
					} );
				} ) );
			}

			return defer2.promise;
		} )
		.then( function() {
			if ( aProm.length > 0 ) {
				return Promise.all( aProm ).then( function( res ) {
					return defer2.resolve();
				}
				,function( err ) {
					logger.error( err );

					return defer2.reject( err );
				} );
			}
			else {
				return defer2.resolve();
			}
		}
		,function( err ) {
			logger.error( err );

			return defer2.reject( err );
		} );

		return Q.resolve();
	};

	/*
	 * function save connections
	 * and groups connections
	 */
	newconfig.saveGroupConnections = function( aConnData ) {
		var defer2 = Q.defer();
		var aProm  = [];

		if ( !aConnData ) {
			return Q.reject( {
				error: 'No Data Connection!'
			} );
		}

		return db.models.t_group.findAndCountAll().then( function( data ) {
			var srvData     = null;
			var aCreateData = [];

			if ( !data || !data.count ) {
				return defer2.reject();
			}

			for ( var i = 0; i < aConnData.length; i++ ) {
				srvData = serverConfig.getServersData( aConnData[i].serverName );

				if ( srvData != null ) {
					if ( !( 'groups' in srvData ) ) {
						aCreateData.push( {
							id_group:      1,
							id_connection: aConnData[i].id_connection
						} );
					}
					else {
						// find in all groups
						for ( var j = 0; j < data.count; j++ ) {
							if( srvData.groups.indexOf( data.rows[j].group_name ) != -1 ) {
								aCreateData.push( { id_group: data.rows[j].id, id_connection: aConnData[i].id_connection } );
							}
						}
					}
				}
			}

			return Q.resolve( aCreateData );
		} )
		.then( function( aData ) {
			// destroy data and create it
			return db.models.t_connection_group.destroy( {
				where: {
				}
			} )
			.then( function() {
				if ( aData.length ) {
					logger.debug ( 'main.sqlite initialize - Ok');

					return db.models.t_connection_group.bulkCreate( aData );
				}
				else {
					return defer2.resolve();
				}
			},
			function( err ) {
				logger.error ( err );

				return Q.reject( err );
			} );
		} );
	};

	/*
	 * function search value
	 * in array of objects
	 */
	function findInArrayObjects( arr, key, value ) {
		var ret = -1;

		if ( !arr || !key ) {
			return ret;
		}

		for ( var i = 0; i < arr.length; i++ ) {
			if ( key in arr[i] ) {
				if ( arr[i][key] == value ) {
					ret = i;
					break;
				}
			}
		}

		return ret;
	}

	module.exports = {
		newconfig : newconfig,
		init      : init
	}
})(module);
