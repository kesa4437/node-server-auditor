'use strict';

(function(module) {
	var Q         = require('q');
	var fs        = require('fs');
	var path      = require('path');
	var util      = require('util');
	var dbManager = require('../../../src/node/database');
	var databases = require('../../../configuration').databases;
	var logger    = require('src/node/log');
	var appDir    = path.dirname(require.main.filename);

	function initialize() {
		return runSerialSQLScripts(buildInitScripts());
	}

	function buildInitScripts() {
		var local_data_storages = Object.keys(databases);
		var sequences           = [];
		var initFile            = null;
		var initConfig          = null;
		var sqlScriptsDir       = null;

		local_data_storages.forEach(function(dbName) {
			initFile = databases[dbName].initialization;

			if (initFile) {
				initConfig = require(initFile);

				sortByOrder(initConfig);

				sqlScriptsDir = path.dirname(initFile);

				sequences = sequences.concat(initConfig.map(function(row) {
					return buildSQLFunction(sqlScriptsDir, row.script, dbName);
				}))
			}
		})

		return sequences;
	}

	function buildSQLFunction(sqlScriptsDir, script, dbName) {
		var sql  = getSQLContent(sqlScriptsDir, script);
		var meta = {
			db:     dbName,
			script: script
		};

		return function() {
			logger.info( 'db: [', dbName, '], script: {', script, '}' );

			return dbManager(dbName)
				.query(sql)
				.catch(onError(meta))
		}
	}

	function getSQLContent(sqlScriptsDir, sqlFile) {
		var strFullSQLInitFile = path.normalize(path.join(appDir, sqlScriptsDir, sqlFile));

		// logger.debug( 'appDir: {', appDir, '}' );
		// logger.debug( 'sqlScriptsDir: {', sqlScriptsDir, '}' );
		// logger.debug( 'sqlFile: {', sqlFile, '}' );
		// logger.debug( 'strFullSQLInitFile: {', strFullSQLInitFile, '}' );

		return fs.readFileSync(strFullSQLInitFile).toString();
	}

	function runSerialSQLScripts(funcs) {
		return funcs.reduce(Q.when, Q([]));
	}

	function onError(data) {
		return function( err ) {
			logger.error( err );
			logger.debug( 'Error message: {', err.message, '}' );
			logger.debug( 'Error running init script: {', data.script, '} for database: [', data.db, ']' );

			process.exit(-1);
		}
	}

	function sortByOrder(data) {
		data.sort(function(a, b) {
			return Number(a.order) - Number(b.order);
		})
	}

	module.exports = initialize;
})(module);
