'use strict';

(function (module) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();
	var t_connection_type        = require('./t_connection_type');

	var t_module = default_database.define('t_module',
	{
		// column 'id_module'
		id_module: {
			field:         'id_module',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'module id - primary key',
			validate:      {
			}
		},

		// column 'module_name'
		module_name: {
			field:     'module_name',
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   'module name - text description',
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'is_active'
		is_active: {
			field:        'is_active',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			comment:      'for soft delete of row',
			defaultValue: true
		},
			 
		id_connection_type: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_connection_type,
					
				// This is the column name of the referenced model
				key: 'id_connection_type'
			}
		}
	}, {
		// define the table's name
		tableName: 't_module',

		comment: 'list of availible modules for connection type',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			t_module.belongsTo(
				models.t_connection_type, {
					foreignKey: 'id_connection_type'
				}
			);
		},

		indexes: [ {
				name:   'uidx_t_module_module_name',
				unique: true,
				fields: [
					'id_connection_type',
					'module_name'
				]
			}
		]
	}
	);

	module.exports = t_module;
})(module);
