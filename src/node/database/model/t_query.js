'use strict';

(function (module) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();

	var TQuery = default_database.define( 't_query', {
		// column 'id_query'
		id_query: {
			field:         'id_query',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'query id - primary key',
			validate:      {
			}
		},

		// column 'query_location'
		query_location: {
			field:     'query_location',
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   'query_location - local path to the file with the query',
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'is_active'
		is_active: {
			field:        'is_active',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			comment:      'true if query is still active (exists)',
			defaultValue: true,
			validate:     {
			}
		}
	}, {
		// define the table's name
		tableName: 't_query',

		comment: 'list of queries',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TQuery.belongsTo(
				models.t_report, {
					foreignKey: 'id_report'
				}
			);
		},

		indexes: [
		]
	}
	);

	module.exports = TQuery;
})(module);
