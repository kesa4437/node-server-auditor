'use strict';

(function (module) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();
	var t_module = require('./t_module');

	var t_report = default_database.define('t_report',
	{
		// column 'id_report'
		id_report: {
			field:         "id_report",
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       "report id - primary key",
			validate:      {
			}
		},

		// column 'report_name'
		report_name: {
			field:     "report_name",
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   "report_name - name of report",
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'is_active'
		is_active: {
			field:        "is_active",
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			comment:      "true if report is still active (exists)",
			defaultValue: true,
			validate:     {
			}
		},
			 
		id_module: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_module,
				
				// This is the column name of the referenced model
				key: 'id_module'
			}
		}
	},
	{
		// define the table's name
		tableName: 't_report',

		comment: "list of reports",

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			t_report.belongsTo(
				models.t_module, {
					foreignKey: 'id_module'
				}
			);
		},

		indexes: [
		]
	}
	);

	module.exports = t_report;
})(module);
