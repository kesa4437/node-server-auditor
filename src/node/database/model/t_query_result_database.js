'use strict';

(function (module) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();

	var t_query_result_database = default_database.define('t_query_result_database',
	{
		// column 'id_result_database'
		id_result_database: {
			field:         "id_result_database",
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       "result database id - primary key"
		},

		// column 'result_database_name'
		result_database_name: {
			field:     "result_database_name",
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   "database name with query results",
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		}
	},
	{
		// define the table's name
		tableName: 't_query_result_database',

		comment: "query results information",

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true
	}
	);

	module.exports = t_query_result_database;
})(module);
