'use strict';

(function (module) {
	var Promise      = require('promise');
	var Sequelize    = require('sequelize');
	var _            = require('lodash');
	var database     = require('src/node/database')();
	var t_query      = require('./t_query');
	var t_report      = require('./t_report');
	var t_module      = require('./t_module');
	var t_connection_type = require('./t_connection_type');
	var logger       = require('src/node/log');
	var nodeSchedule = require('node-schedule');

	var t_connection_query_schedule = database.define('t_connection_query_schedule',
	{
		// column 'id'
		id: {
			field:         'id',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'schedule id - primary key',
			validate:      {
			}
		},

		// column 'groupName'
		groupName: {
			field:     'groupName',
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   'external server group of connections name',
			validate: {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'forGroup'
		forGroup: {
			field:        'forGroup',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			defaultValue: false,
			comment:      'is execution schedule for group of connections',
			validate:     {
			}
		},

		// column 'schedule'
		schedule: {
			field:     'schedule',
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   'execution schedule, like {30 * * * * *} - execute report every minute at 30 second',
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'isEnabled'
		isEnabled: {
			field:        'isEnabled',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			defaultValue: false,
			comment:      'is execution schedule enabled',
			validate:     {
			}
		},
			 
		id_query: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_query,
					
				// This is the column name of the referenced model
				key: 'id_query'
			}
		}
	}, {
		// define the table's name
		tableName: 't_connection_query_schedule',

		comment: 'list of reports execution schedules',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			t_connection_query_schedule.belongsTo(
				models.t_query, {
					foreignKey: 'id_query'
				}
			);
		},
		
		indexes: [
			{
				name:   'uidx_t_connection_query_schedule',
				unique: true,
				fields: [
					'schedule',
					'id_query'
				]
			}
		]
	}
	);

	t_connection_query_schedule.sync({ force: false }).then(function () {
		var activeSchedule;
		var jobName;
		var promises = [];
		var Report   = require('src/node/com/report')('start');

		t_connection_query_schedule.findAll({
			include: [
				{
					model: t_query,
					include: [
						{
							model: t_report,
							include: [
								{
									model: t_module,
									include: [
										{
											model: t_connection_type
										}
									]
								}
							]
						}
					]
				}
			]
		}).then(function(result) {
			result.forEach(function(item) {
				if ( !item.isEnabled ) {
					return;
				}

				item.repName = item.t_query.t_report.report_name;
				item.moduleType = item.t_query.t_report.t_module.module_name;
				item.connType = item.t_query.t_report.t_module.t_connection_type.connection_type_name; 
				item.connName = item.t_query.t_report.t_module.t_connection_type.connection_type_name; 
					
				if( !item.forGroup ){
					promises.push( new Promise( function( resolve, reject ) {

					jobName = getCurrNameJob(item);

					activeSchedule = nodeSchedule.scheduleJob(jobName, item.schedule, function () {
						Report.getReportByScheduler(item.repName, item.connType, item.connName, item.moduleType, true)
							.then(data => {
								return resolve(data);
							})
							.catch(err => {
								logger.error ( err );

								return reject(err);
							})
						});
					}));
				}else{
					jobName = getCurrNameJob( item );
					database.models.t_group.findOne( {
						where: {
							group_name: item.groupName
						}
					} ).then( function( rData ) {
						database.models.t_connection_group.findAndCountAll( {
							where: {
								id_group: rData.id
							}
							,include: [
								{
									model: database.models.t_connection
								}
							]
						} ).then( function( aData ){
							if( aData.count ){
								for( var i = 0; i < aData.count; i++ ){
									promises.push( new Promise( function( resolve, reject ){
										var connName2 = _.clone( aData.rows[i].dataValues.t_connection.dataValues.serverName );

										active_schedule = nodeSchedule.scheduleJob( jobName, item.schedule, function() {
											Report.getReportByScheduler( item.repName, item.connType, connName2, item.moduleType, true )
												.then( function( repData ) {
													resolve( repData );
												},
												function( err ) {
													reject( err );
												} );
											});
									} ) );
								}
							}
							else {
								return dfd.reject();
							}
						} );
					} );
				}
			});
		})
		.catch( function(err) {
			
		})
		.finally(function() {

		});
	});

	function getCurrNameJob( record ) {
		return record.connType + record.connName + record.moduleType + record.repName + record.schedule;
	};

	function extractItem(item) {
		var dataValues = item.dataValues;
		var resItem    = {};

		for ( var key in dataValues ) {
			if ( dataValues.hasOwnProperty(key) ) {
				resItem[key] = dataValues[key];
			}
		}

		return resItem;
	};

	module.exports = t_connection_query_schedule;
})(module);
