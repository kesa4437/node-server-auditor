'use strict';

(function (module) {
	var Sequelize = require('sequelize');
	var database  = require('src/node/database')();

	var t_query_parameter = database.define('t_query_parameter',
		{
			// column 'id_query_parameter'
			id_query_parameter: {
				field:         "id_query_parameter ",
				type:          Sequelize.INTEGER,
				allowNull:     false,
				primaryKey:    true,
				autoIncrement: true,
				comment:       'query_parameter id  - primary key',
				validate:      {
				}
			},

			// column 'nameParam'
			nameParam: {
				field:        'nameParam',
				type:         Sequelize.STRING,
				allowNull:    true,
				defaultValue: '',
				comment:      'name of the parameter',
			},

			// column 'paragraphName'
			paragraphName: {
				field:        'paragraphName',
				type:         Sequelize.STRING,
				allowNull:    true,
				defaultValue: '',
				comment:      'paragraph name',
				validate:     {
				}
			},

			// column 'sectionName'
			sectionName: {
				field:        'sectionName',
				type:         Sequelize.STRING,
				allowNull:    true,
				defaultValue: '',
				comment:      'section name',
				validate:     {
				}
			},

			// column 'is_active'
			is_active: {
				field:        "is_active",
				type:         Sequelize.BOOLEAN,
				allowNull:    false,
				defaultValue: true,
				comment:      "is active",
				validate:     {
				}
			},

			// column 'query_parameter_name'
			query_parameter_name: {
				field:        "query_parameter_name",
				type:         Sequelize.STRING,
				allowNull:    true,
				defaultValue: "",
				comment:      "query parameter name",
				validate:     {
				}
			}
		}, {
			// define the table's name
			tableName: 't_query_parameter',

			comment: 'table changed parameter for reports',

			// 'createdAt' to actually be called '_datetime_created'
			createdAt: '_datetime_created',

			// 'updatedAt' to actually be called '_datetime_updated'
			updatedAt: '_datetime_updated',

			// disable the modification of table names; By default, sequelize will automatically
			// transform all passed model names (first parameter of define) into plural.
			freezeTableName: true,

			charset: 'utf8',

			underscored: true,

			associate: function( models ) {
				t_query_parameter.belongsTo(
					models.t_query, {
						foreignKey: 'id_query'
					}
				);
			},

			indexes: [ {
					name:   'uidx_t_query_parameter',
					unique: false,
					fields: [
						'nameParam'
					]
				}
			]
		}
	);

	module.exports = t_query_parameter;
})(module);
