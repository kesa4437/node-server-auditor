'use strict';

( function( module ) {
	var Sequelize = require( 'sequelize' );
	var database  = require( 'src/node/database' )();

	// table t_connection_group
	var t_connection_group = database.define( 't_connection_group', {
			// column 'is_active'
			is_active: {
				field:        'is_active',
				type:         Sequelize.BOOLEAN,
				allowNull:    false,
				comment:      'for soft delete of row',
				defaultValue: true
			}
		}, {
			// define the table's name
			tableName: 't_connection_group',

			comment: 'groups of connections',

			// disable the modification of table names; By default, sequelize will automatically
			// transform all passed model names (first parameter of define) into plural.
			freezeTableName: true,

			charset: 'utf8',

			underscored: true,

			associate: function( models ) {
				t_connection_group.belongsTo(
					models.t_connection, {
						foreignKey: 'id_connection'
					}
				);

				t_connection_group.belongsTo(
					models.t_group, {
						foreignKey: 'id_group'
					}
				);
			},

			indexes: [ {
					name:   'uidx_t_connection_group',
					unique: true,
					fields: [
						'id_group',
						'id_connection'
					]
				}
			]
		}
	);

	module.exports = t_connection_group;
} )( module );
