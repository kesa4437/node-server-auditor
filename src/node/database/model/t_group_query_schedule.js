'use strict';

( function( module ) {
	var Sequelize = require( 'sequelize' );
	var database  = require( 'src/node/database' )();

	var t_group_query_schedule = database.define( 't_group_query_schedule', {
			// column 'id'
			id: {
				 field:         'id'
				,type:          Sequelize.INTEGER
				,allowNull:     false
				,primaryKey:    true
				,autoIncrement: true
				,comment:       'result table id - primary key'
			},

			/*
			// column 'id_query'
			id_query: {
				 field:         'id_query'
				,type:          Sequelize.INTEGER
				,allowNull:     false
				,primaryKey:    false
				,autoIncrement: false
				,comment:       'record set for the query'
				,validate:      {
				}
			},

			// column 'id_group'
			id_group: {
				 field:         'id_group'
				,type:          Sequelize.INTEGER
				,allowNull:     false
				,primaryKey:    false
				,autoIncrement: false
				,comment:       'set to group of connection'
				,validate:      {
				}
			},
			*/

			// column 'schedule'
			schedule: {
				field:      'schedule',
				type:       Sequelize.STRING,
				allowNull:  false,
				primaryKey: false,
				comment:    'schedule query',
			}
		}, {
			// define the table's name
			tableName: 't_group_query_schedule',

			comment: 'schedule query for the group of servers',

			// disable the modification of table names; By default, sequelize will automatically
			// transform all passed model names (first parameter of define) into plural.
			freezeTableName: true,

			charset: 'utf8',

			underscored: true,

			associate: function( models ) {
				t_group_query_schedule.belongsTo(
					models.t_query, {
						foreignKey: 'id_query'
					}
				);
				t_group_query_schedule.belongsTo(
					models.t_group, {
						foreignKey: 'id_group'
					}
				);
			},

			indexes: [ {
					name:   'uidx_t_group_query_schedule',
					unique: true,
					fields: [
						'id_query',
						'id_group'
					]
				}
			]
		}
	);

	module.exports = t_group_query_schedule;
} )( module );
