'use strict';

( function( module ) {
	var Sequelize = require( 'sequelize' );
	var database  = require( 'src/node/database' )();

	// table t_group
	var t_group = database.define( 't_group', {
			// column 'id'
			id: {
				field:         'id',
				type:          Sequelize.INTEGER,
				allowNull:     false,
				primaryKey:    true,
				autoIncrement: true,
				comment:       'query_parameters id - primary key',
				validate:      {
				}
			},

			// column 'group_name'
			group_name: {
				field:         'group_name',
				type:          Sequelize.STRING,
				allowNull:     false,
				comment:       'name of connection group',
				validate:      {
					notEmpty: true //don't allow empty string
				}
			},

			// column 'is_active'
			is_active: {
				field:        'is_active',
				type:         Sequelize.BOOLEAN,
				allowNull:    false,
				comment:      'for soft delete of row',
				defaultValue: true
			}
		}, {
			// define the table's name
			tableName: 't_group',

			comment: 'list of connection groups',

			// disable the modification of table names; By default, sequelize will automatically
			// transform all passed model names (first parameter of define) into plural.
			freezeTableName: true,

			charset: 'utf8',

			underscored: true,
		}
	);

	t_group.sync( { force: false } );

	module.exports = t_group;
} )( module );
