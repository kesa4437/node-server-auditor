'use strict';

(function (module) {
	var handler = null;
	var express = require('express');
	var router  = express.Router();
	var Data    = require('src/node/com/data');
	var Report  = require('src/node/com/report');
	var logger  = require('src/node/log');

	router.get('/', function (req, res) {
		var list = Report(req.sessionId).list();

		if ( list ) {
			res.json(handler.response(list));
		}
		else {
			res.json(handler.error('not configured'));
		}
	});

	router.get('/json', function (req, res) {
		var list = Report(req.sessionId).jsonMenu();

		if (list) {
			res.json(handler.response(list));
		}
		else {
			res.json(handler.error('not configured'));
		}
	});

	router.get('/:name', function (req, res) {
		logger.debug( '[32]index.js:router.get' );

		var recent = req.query.recent === 'true';

		Report(req.sessionId).getReport(req.params.name, recent).then(function (result) {
			logger.debug( '[37]index.js:router.get:req.params.name: {', req.params.name, '}' );
			logger.debug( '[38]index.js:router.get:recent: {', recent, '}' );

			res.json(handler.response(result));
		}, function ( err ) {
			logger.error( err );
			logger.error( '[43]index.js:router.get' );

			res.json(handler.error( 'report error:', err ));
		});
	});

	module.exports = function (resHandler) {
		handler = resHandler;

		return router;
	};
})(module);
