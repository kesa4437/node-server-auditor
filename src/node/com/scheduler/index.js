'use strict';

(function (module) {
	var Q            = require('q');
	var async        = require('async');
	var Promise      = require('promise');
	var _            = require('lodash');
	var dbsched      = require('src/node/database/model');
	var Report       = require('src/node/com/report');
	var database     = require('src/node/database')();
	var logger       = require('src/node/log');
	var nodeSchedule = require('node-schedule');

	var t_connection_query_schedule = require('src/node/database/model/t_connection_query_schedule');
	var t_query = require('src/node/database/model/t_query');
	var t_report = require('src/node/database/model/t_report');
	var t_module = require('src/node/database/model/t_module');
	var t_connection_type = require('src/node/database/model/t_connection_type');

	/**
	 * Report class
	 *
	 */
	function Scheduler() {
		this._arr_sched = [];
	}

	/**
	 * Enable determined task
	 *
	 * @param {mixed} record It may be record Object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.enable = function(record, sessionId) {
		var active_schedule = null;
		var recent          = true;
		var dfd             = Q.defer();

		enableByRecord = enableByRecord.bind(this);

		if ( typeof record === 'object' ) {
			enableByRecord(record);
		}
		else {
			this.getRecord(record).then(enableByRecord, dfd.reject);
		}

		return dfd.promise;

		function enableByRecord (record) {
			t_connection_query_schedule.findById(
				record.id,
				{
					include: [
						{
							model: t_query,
							include: [
								{
									model: t_report,
									include: [
										{
											model: t_module,
											include: [
												{
													model: t_connection_type
												}
											]
										}
									]
								}
							]
						}
					]
				}
			)
			.then(function(connection_query_schedule) {
				if (connection_query_schedule) {
					var result = {};
					
					result.id = connection_query_schedule.id;
					result.connType = connection_query_schedule.t_query.t_report.t_module.t_connection_type.connection_type_name; 
					result.connName = connection_query_schedule.t_query.t_report.t_module.t_connection_type.connection_type_name; 
					result.moduleType = connection_query_schedule.t_query.t_report.t_module.module_name;
					result.repName = connection_query_schedule.t_query.t_report.report_name;
					result.isEnabled = connection_query_schedule.isEnabled;
					result.schedule = connection_query_schedule.schedule;
					
					var repName    = result.repName;
					var connType   = result.connType;
					var connName   = result.connName;
					var moduleType = result.moduleType;
					var schedule   = result.schedule;
					var nameJob    = getCurrNameJobPtr(result);
					
					if( !record.forGroup ){
						active_schedule = nodeSchedule.scheduleJob(nameJob, schedule, function() {
							Report(sessionId).getReportByScheduler(repName, connType, connName, moduleType, recent)
								.then(data => {
									return dfd.resolve(data);
								})
								.catch(err => {
									logger.error ( err );
			
									return dfd.reject(err);
								})
						});
					}
					else {
						dbsched.t_group.findOne( {
							where: {
								group_name: record.groupName
							}
						} ).then( function( rData ){
							dbsched.t_connection_group.findAndCountAll( {
								where: {
									id_group: rData.id
								}
								,include: [ {
										model: dbsched.t_connection
									}
								]
							} ).then( function( aData ){
								var aProm = [];

								if( aData.count ){
									for( var i = 0; i < aData.count; i++ ){
										aProm.push( new Promise( function( resolve, reject ){
											var connName2 = _.clone( aData.rows[i].dataValues.t_connection.dataValues.serverName );
											active_schedule = nodeSchedule.scheduleJob( nameJob, schedule, function(){
												Report( sessionId ).getReportByScheduler( repName, connType, connName2, moduleType, recent )
													.then( function( repData ){
														resolve( repData );
													},
													function( err ){
														reject( err );
													} );
											});
										} ) );
									}

									if( aProm.length ){
										return Promise.all( aProm ).then( function(){
											return dfd.resolve();
										} );
									}
								}
								else {
									return dfd.reject();
								}
							} );
						} );
					}
					updateSelectorPtr(result, true)
						.then(dfd.resolve, dfd.reject);
				}
			});
		}
	};

	/**
	 * Disable determined task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.disable = function (record) {
		var dfd  = Q.defer();
		var jobs = this.getStartedJobs();
		
		disableByRecord = disableByRecord.bind(this);

		if ( typeof record === 'object' ) {
			disableByRecord(record);
		}
		else {
			this.getRecord(record).then(disableByRecord, dfd.reject);
		}

		return dfd.promise;

		function disableByRecord (record) {
			t_connection_query_schedule.findById(
				record.id,
				{
					include: [
						{
							model: t_query,
							include: [
								{
									model: t_report,
									include: [
										{
											model: t_module,
											include: [
												{
													model: t_connection_type
												}
											]
										}
									]
								}
							]
						}
					]
				}
			)
			.then(function(connection_query_schedule) {
				if (connection_query_schedule) {
					var result = {};
					
					result.id = connection_query_schedule.id;
					result.connType = connection_query_schedule.t_query.t_report.t_module.t_connection_type.connection_type_name; 
					result.connName = connection_query_schedule.t_query.t_report.t_module.t_connection_type.connection_type_name; 
					result.moduleType = connection_query_schedule.t_query.t_report.t_module.module_name;
					result.repName = connection_query_schedule.t_query.t_report.report_name;
					result.isEnabled = connection_query_schedule.isEnabled;
					result.schedule = connection_query_schedule.schedule;
					
					var currJobName = getCurrNameJobPtr(result);
	
					if (jobs.hasOwnProperty(currJobName) ) {
						jobs[currJobName].cancel();
		
						updateSelectorPtr(result, false)
							.then(dfd.resolve, dfd.reject);
					}
					else {
						dfd.reject({'result': 'job not found'});
					}
				}
			});
		};
	};

	function getCurrNameJobPtr(record) {
		return record.connType + record.connName + record.moduleType + record.repName + record.schedule;
	};

	Scheduler.prototype.getStartedJobs = function() {
		return nodeSchedule.scheduledJobs;
	};

	Scheduler.prototype.list = function () {
		var tables = [];
		var dfd    = Q.defer();
		
		t_connection_query_schedule.findAll({
			include: [
				{
					model: t_query,
					include: [
						{
							model: t_report,
							include: [
								{
									model: t_module,
									include: [
										{
											model: t_connection_type
										}
									]
								}
							]
						}
					]
				}
			]
		}).then(function(table) {
			var result = table.map(extractItem);

			if (result) {
				tables.push(result);
			}

			dfd.resolve(tables);
		})
		.finally(function() {
			dfd.resolve(tables);
		});
	

		return dfd.promise;

		function extractItem(item) {
			var dataValues = item.dataValues;
			var resItem    = {};
			
			for (var key in dataValues) {
				if (dataValues.hasOwnProperty(key)) {
					resItem[key] = dataValues[key];
				}
			}
			resItem.repName = dataValues.t_query.t_report.report_name;
			resItem.moduleType = dataValues.t_query.t_report.t_module.module_name;
			resItem.connType = dataValues.t_query.t_report.t_module.t_connection_type.connection_type_name; 
			resItem.connName = dataValues.t_query.t_report.t_module.t_connection_type.connection_type_name; 
 			
			return resItem;
		};
	};

	Scheduler.prototype.insert = function(record) {
		var dfd = Q.defer();
		
		dbsched.t_connection_query_schedule.sync().then(function () {
			var promises = [];

			// connType:   record.connType,
			// connName:   record.connName,
			// moduleType: record.moduleType,
			// repName:    record.repName,
			// TODO SSK t_connection_query_schedule should contains t_report not t_query.
				
			promises.push( new Promise( function( resolve, reject ) {
				dbsched.t_connection_type.findOrCreate( {
					where: {
						connection_type_name:    record.connName
					}
				}).spread( function( connType, created ) {
					dbsched.t_module.findOrCreate( {
						where: {
							module_name: record.moduleType,
							id_connection_type: connType.id_connection_type
						}
					}).spread( function( module, created ) {
						dbsched.t_report.findOrCreate( {
							where: {
								report_name:    record.repName,
								id_module:      module.id_module
							}
						}).spread( function( report, created ) {
							dbsched.t_connection_query_schedule.create({
								schedule:   record.schedule,
								isEnabled:  record.isEnabled,
								id_query: report.id_report,
								groupName:  record.groupName,
								forGroup:   record.forGroup
							}).then( function(res) {
								record.id = res.id;
								resolve(record);
							}).catch(function(err) {
								reject(err);
							});
						});
					});
				});
			}));

			Promise.all( promises ).then( function( resp ) {
				dfd.resolve(resp);
			}, function( err ) {
				dfd.reject(err);
			});
		});

		return dfd.promise;
	};

	Scheduler.prototype.update = function (id, newRecord, sessionId) {
		var enablePtr = this.enable;
		newRecord.id = id;
		
		return this.getRecord(id).then(function (record) {
			this.disable(record)
				.then(function () {
					return dbsched.t_connection_type.findOrCreate( {
						where: {
							connection_type_name:    newRecord.connName
						}
					}).spread( function( connType, created ) {
						dbsched.t_module.findOrCreate( {
							where: {
								module_name: newRecord.moduleType,
								id_connection_type: connType.id_connection_type
							}
						}).spread( function( module, created ) {
							dbsched.t_report.findOrCreate( {
								where: {
									report_name:    newRecord.repName,
									id_module:      module.id_module
								}
							}).spread( function( report, created ) {
								newRecord.id_query = report.id_report;
								record.updateAttributes(newRecord)
								.then(function(result) {
									enablePtr(newRecord, sessionId);
								}.bind(this));
							});
						});
					});
				}.bind(this), function () {
					record.updateAttributes(newRecord);
				});
		}.bind(this));
	};

	Scheduler.prototype.delete = function (id) {
		var dfd = Q.defer();
		
		this.getRecord(id).then(function (record) {
			this.disable(record).finally(function () {
				dbsched.t_connection_query_schedule.sync().then(function () {
					dbsched.t_connection_query_schedule.destroy({
						where: {
							id: id
						}
					}).then(dfd.resolve, dfd.reject);
				}, dfd.reject);
			});
		}.bind(this), dfd.reject);

		return dfd.promise;
	};

	function updateSelectorPtr(record, selector) {
		var dfd = Q.defer();
		
		dbsched.t_connection_query_schedule.sync().then(function () {
			var promises = [];

			promises.push(function (next) {
				dbsched.t_connection_query_schedule.find( { where:{ id :  record.id } })
					.then(function (record) {
						if ( record ) {
							record.updateAttributes({
								isEnabled: selector
							}).catch(function( err ) {
								dfd.reject(err);
							});
						}
					}).finally(next);;
			});

			async.auto(promises, function (err, results) {
				if (err) {
					dfd.reject(err);
				}
				else {
					dfd.resolve(results);
				}
			});
		});

		return dfd.promise;
	};

	Scheduler.prototype.getRecord = function (id) {
		var dfd = Q.defer();

		dbsched.t_connection_query_schedule.findById(id).then(function (record) {
			if (record) {
				dfd.resolve(record);
			}
			else {
				dfd.reject('record not found');
			}
		}, dfd.reject);

		return dfd.promise;
	};

	/*
	 * a storage for all schedulers
	 */
	var scheduler_instances = { };

	/*
	 * a factory for report instances
	 */
	module.exports = function (sessionId) {
		if (!(sessionId in scheduler_instances)) {
			scheduler_instances[sessionId] = new Scheduler();
		}

		return scheduler_instances[sessionId];
	};
})(module);
