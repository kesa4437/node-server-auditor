'use strict';

(function(module) {
	var Filters = {
		validateDataSets: require('./validateDataSets'),
		delArrayDataSets: require('./delArrayDataSets')
	};

	module.exports = Filters;
})(module);
