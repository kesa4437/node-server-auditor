'use strict';

(function (module) {
	var delArrayDataSets = function (tables) {
		if (tables.length && Array.isArray(tables[0])) {
			tables = tables[0];
		}

		return tables;
	};

	module.exports = delArrayDataSets;
})(module);
