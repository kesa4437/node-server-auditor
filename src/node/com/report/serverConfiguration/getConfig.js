'use strict';

(function (module) {
	var path       = require('path');
	var validator  = require('validator');
	var logger     = require('src/node/log');
	var Data       = require('src/node/com/data');
	var appDir     = path.dirname(require.main.filename);
	var serversDir = path.join(appDir, 'servers');

	/**
	 * Getting configuration from chosen server
	 * @param {string} name of server
	 * @param {string} group of server
	 * @returns {object Object}
	 */
	module.exports = function ( name, group ) {
		var config  = null;
		var prefs   = null;
		var version = null;

		try {
			prefs = require(path.join( serversDir, validator.whitelist( name, 'A-Za-z0-9_.-' )));

			prefs = prefs.connection;

			config = new Data.ServerConfiguration( name, prefs, version, group );
		}
		catch ( err ) {
			logger.error( err );
			logger.debug( 'name: {', name, '}' );
			logger.debug( 'getConfig' );

			return;
		}

		return config;
	};
})(module);
