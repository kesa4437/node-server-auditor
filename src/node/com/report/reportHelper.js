'use strict';

const REPORT_REGULAR     = 'REPORT_REGULAR'
const REPORT_BY_SCHEDULE = 'REPORT_BY_SCHEDULE'

const Q         = require('q');
const _         = require('lodash');
const Connector = require('src/node/com/report/connector/');
const SnapShot  = require('src/node/com/report/snapShot');
const Filters   = require('src/node/com/report/filters');
const Database  = require('src/node/database');
const Data      = require('src/node/com/data');
const logger    = require('src/node/log');
const extend    = require('extend');

function getReport(opts) {
	// OPTS destructuring
	const OPTS     = validateAndBuild(opts);
	const ctx      = OPTS.ctx;
	const name     = OPTS.name;
	const recent   = OPTS.recent;
	const module   = OPTS.module;
	const report   = OPTS.report;
	const server   = OPTS.server;
	const connType = OPTS.connType;

	if ( OPTS.error ) {
		return Q.reject({ error:OPTS.error });
	}

	const request = report.get('request');

	if (recent) {
		return recentReport();
	}
	else {
		return nonRecentReport(server, report, module)
			.then (data => convertDataToReport   (data, report))
			.catch(err  => onNonRecentReportError(err,  report))
	}

	// -----------------------------------------------------------------------------------------------

	function recentReport() {
		return ctx.loadParamsFromReport(name, module, server, request)
			.then (params => processReportParams(params))
			.catch(onReportError)
	}

	function processReportParams(params) {
		let prefs = server.get('prefs');

		if ( params && 'request' in params ) {
			let databaseName = params.databaseName;

			if ( databaseName !== '' ) {
				prefs = replacePref(module.get('type'), prefs, databaseName)
			}
		}

		const connectorType = (connType === undefined) ? module.get('type') : connType;
		const connector     = new Connector[connectorType](prefs);

		return afterProcessReportParams(connector, params, report, module, server);
	}
}

function getServer(opts) {
	const ctx = opts.ctx

	switch(opts.type) {
		case REPORT_REGULAR:
			return ctx.config.get('server');
		case REPORT_BY_SCHEDULE:
			return ctx.server(opts.connName);
	}
}

function getModule(opts) {
	const ctx = opts.ctx

	switch(opts.type) {
		case REPORT_REGULAR:
			return ctx.module();
		case REPORT_BY_SCHEDULE:
			return ctx.moduleFind(opts.moduleType);
	}
}

function replacePref(type, pref, valueDatabase) {
	switch(type) {
		case 'mssql':
		case 'mysql':
			if ( 'database' in pref ) {
				pref.database = valueDatabase;

				if ( 'options' in pref ) {
					pref.options.database = valueDatabase;
				}
			}
			break;
	}

	return pref;
}

function buildSnapshot(report, module, server, dataSet) {
	return SnapShot.set(Database(report.get('storage')), {
		module      : module,
		name        : report.get('name'),
		primaryKeys : report.get('primaryKeys'),
		saveHistory : report.get('saveHistory'),
		serverName  : server.get('name')
	},
	dataSet);
}

function buildSnapshotParams(server, report, module) {
	return {
		module      : module,
		name        : report.get('name'),
		primaryKeys : report.get('primaryKeys'),
		serverName  : server.get('name')
	};
}

function onReportError(err) {
	logger.error('error - ', err);

	return Q.reject(err);
}

function convertDataToReport(dataSet, report) {
	const data   = dataSet.slice(0);
	const result = new Data.ReportResult(report, data);

	return result.data();
}

function nonRecentReport(server, report, module) {
	const queryShow      = report.get('queryShow');
	const reportStorage  = Database(report.get('storage'));
	const snapShotParams = buildSnapshotParams(server, report, module);

	if (queryShow) {
		return SnapShot.queryShow(reportStorage, snapShotParams, queryShow);
	}

	return SnapShot.get(reportStorage, snapShotParams);
}

function afterProcessReportParams(connector, params, report, module, server) {
	return connector.query(params.request)
		.then(tablesData => {
			const queryShow    = report.get('queryShow')
			const tables       = Filters.validateDataSets(tablesData)
			const storePromise = buildSnapshot(report, module, server, tables)

			// require promise fulfil only in case of historical reports (which depends on the data saved)
			return queryShow ? storePromise : tables;
		})
}

function validateAndBuild(opts) {
	if (opts == null) {
		return {
			error: '{opts} should not be empty'
		};
	}

	let   error;
	let   report;
	let   connType = opts.connType;
	const name     = opts.name;
	const server   = getServer(opts);
	const module   = getModule(opts);

	try {
		if ( !server ) {
			return buildResult( 'Connection name not exists' );
		}

		if ( !module ) {
			return buildResult( 'no report module required' );
		}

		connType = _.isUndefined(connType) ? module.get('type') : connType;

		if ( !Connector[connType] ) {
			return buildResult('connector type not exists');
		}

		report = module.get('reports').find(name);

		if ( !report ) {
			return buildResult('report(' + name + ') not found');
		}
	}
	catch (err) {
		error = err;
	}

	return buildResult()

	function buildResult(err) {
		if (err) {
			error = err;

			logger.debug('-----------------------------------------------------------------------------');
			logger.debug('name',     name)
			logger.debug('connType', connType)
			logger.debug('server',   server)
			logger.debug('module',   module)
			logger.debug('report',   report)
			logger.debug('-----------------------------------------------------------------------------');
		}

		return { name, error, server, module, report, connType };
	}
}

function onNonRecentReportError(err, report) {
	const result = new Data.ReportResult(report, err);

	Q.reject(result.data());
}

module.exports = {
	getReport,
	REPORT_BY_SCHEDULE,
	REPORT_REGULAR
};
