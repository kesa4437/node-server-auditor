'use strict';

(function (module) {
	var Q                     = require('q');
	var DefineModelFromSchema = require('./defineModelFromSchema');

	module.exports = function (database, name, primaryKeys) {
		var dfd     = Q.defer();
		var postfix = 's';

		database.getQueryInterface().describeTable(name + postfix).then(function (schema) {
			var skipKeys = require('./skipKeys');
			var result   = {};
			var model    = null;

			for ( var key in schema ) {
				if (schema.hasOwnProperty(key) && skipKeys.indexOf(key) === -1) {
					result[key] = schema[key];
				}
			}

			model = DefineModelFromSchema(database, name, result, primaryKeys);

			dfd.resolve(model);

		}, function (err) {
			dfd.reject( err );
		});

		return dfd.promise;
	};
})(module);
