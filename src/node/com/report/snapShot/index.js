'use strict';

(function (module) {
	var SnapShot = {
		get:       require('./getSnapShot'),
		set:       require('./setSnapShot'),
		queryShow: require('./queryShow'),
		transform: require('./setTransform')
	};

	module.exports = SnapShot;
})(module);
