'use strict';

/*
transformation
*/
(function( module ) {
	var modActs = require('./modelActions');

	module.exports = function( database, query, params ) {
		database.sync().then( function() {
			var topName     = params.module.get('type') + params.module.get('name') + params.name;
			var postfix     = 's';
			var tableName   = new RegExp( '\\$\\{' + params.name + '\\}\\{(\\d+)\\}\\$', 'g' );
			var queryShowRe = query.replace( tableName, topName + '$1' + postfix );

			return modActs.getConnection( params.serverName, true ).then( function( connection ) {
				if ( queryShowRe.indexOf( '_id_connection' ) == -1 ) {
					return database.query( queryShowRe );
				}
				else {
					return database.query( queryShowRe, {
						bind: { _id_connection: connection.get('id_connection') }
					} );
				}
			} );
		} );
	};
})( module );
