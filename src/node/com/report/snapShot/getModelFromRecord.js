'use strict';

(function (module) {
	var Q                     = require('q');
	var Sequelize             = require('sequelize');
	var DefineModelFromSchema = require('./defineModelFromSchema');

	var types = function (value) {
		switch (true) {
			case typeof value === 'number':
				return Sequelize.FLOAT;
			case value instanceof Date:
				return Sequelize.DATE;
			default:
				return Sequelize.TEXT;
			}
		},
		getSchema = function (tableBody) {
			var obj    = null;
			var result = {};

			if ( !tableBody || tableBody[0] === undefined ) {
				return null;
			}

			obj = tableBody[0];

			for ( var key in obj ) {
				result[key] = types(obj[key]);
			}

			return result;
		};

	module.exports = function (database, name, tableBody, primaryKeys) {
		var dfd    = Q.defer();
		var schema = getSchema(tableBody);
		var model  = null;

		if (schema) {
			model = DefineModelFromSchema(database, name, schema, primaryKeys);

			dfd.resolve(model);
		}
		else {
			dfd.reject(schema);
		}

		return dfd.promise;
	};
})(module);
