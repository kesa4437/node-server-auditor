'use strict';

(function (module) {
	var util  = require('util');
	var Model = require('./');

	var ReportResult = function (reportItem, result) {
		ReportResult.super_.apply(this, arguments);

		this.set('name',        reportItem.get('name'));
		this.set('component',   reportItem.get('component'));
		this.set('description', reportItem.get('description'));
		this.set('result',      result);
	};

	util.inherits(ReportResult, Model);

	module.exports = ReportResult;
})(module);
