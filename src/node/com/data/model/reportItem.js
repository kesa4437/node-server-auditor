'use strict';

(function (module) {
	var util  = require('util');
	var Model = require('./');

	var ReportItem = function (params, query, queryShow, query_trans) {
		ReportItem.super_.apply(this, arguments);

		this.set('name',          params.name);
		this.set('component',     params.component);
		this.set('description',   params.description);
		this.set('primaryKeys',   params['primary key']);
		this.set('saveHistory',   params.savehistoryrecords);
		this.set('storage',       params.storage);
		this.set('parameters',    params.parameters);
		this.set('request',       query);
		this.set('request_trans', query_trans); // request transformation
		this.set('queryShow',     queryShow);
	};

	util.inherits(ReportItem, Model);

	module.exports = ReportItem;
})(module);
