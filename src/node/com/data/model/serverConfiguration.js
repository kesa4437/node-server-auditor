'use strict';

(function (module) {
	var util  = require('util');
	var Model = require('./');

	var ServerConfiguration = function( name, prefs, version, group ) {
		ServerConfiguration.super_.apply( this, arguments );

		this.set( 'name',    name );
		this.set( 'prefs',   prefs );
		this.set( 'version', version || 0 );
		this.set( 'group',   group || '' );
	};

	util.inherits( ServerConfiguration, Model );

	module.exports = ServerConfiguration;
})(module);
