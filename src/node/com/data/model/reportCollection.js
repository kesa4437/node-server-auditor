'use strict';

(function (module) {
	var util       = require('util');
	var Collection = require('./collection');

	var ReportCollection = function () {
		var Data = require('src/node/com/data');

		ReportCollection.super_.apply(this, arguments);

		this.requiredType = Data.ReportItem;
	};

	util.inherits(ReportCollection, Collection);

	ReportCollection.prototype.find = function (name) {
		for ( var i = 0; i < this.container.length; i++ ) {
			if ( this.container[i].get('name') === name ) {
				return this.container[i];
			}
		}
	};

	module.exports = ReportCollection;
})(module);
