'use strict';

var log4js = require('log4js');
var path   = require('path');
var appDir = path.dirname(require.main.filename);

log4js.configure('log4js_configuration.json', { cwd: appDir });

var logger = log4js.getLogger('Debug');

Object.defineProperty(exports, 'LOG', { value: logger });
