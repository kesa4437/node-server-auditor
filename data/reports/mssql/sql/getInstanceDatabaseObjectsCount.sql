SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

SELECT
	 tSO.[type_desc]        AS [TypeDesc]
	,tSO.[is_ms_shipped]    AS [IsMSShipped]
	,CASE
		WHEN ((DB_NAME() IN (N'master', N'msdb', N'model')) AND (tSO.[is_ms_shipped] = 0)) THEN
			1
		ELSE
			0
	END                     AS [IsIssue]
	,COUNT(*)               AS [TypeCount]
	,MIN(tSO.[create_date]) AS [CreateDateMin]
	,MAX(tSO.[create_date]) AS [CreateDateMax]
FROM
	[sys].[objects] tSO
GROUP BY
	 tSO.[type_desc]
	,tSO.[is_ms_shipped]
ORDER BY
	 tSO.[type_desc]
	,tSO.[is_ms_shipped]
;
