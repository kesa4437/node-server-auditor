SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

DECLARE
	 @MasterCompatibilityLevel [INTEGER]
	,@MasterCollation          [NVARCHAR](128)
;

SELECT TOP 1
	 @MasterCompatibilityLevel = tSD.[cmptlevel]
	,@MasterCollation          = CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'Collation'))
FROM
	[master].[dbo].[sysdatabases] tSD
WHERE
	tSD.[name] = N'master';

SELECT
	 @@ServerName                                                                        AS [ServerName]
	,tSD.[dbid]                                                                          AS [DatabaseId]
	,tSD.[name]                                                                          AS [DatabaseName]
	,NULL                                                                                AS [DatabaseSourceName]
	,tSD.[cmptlevel]                                                                     AS [DatabaseCompatibilityLevel]
	,@MasterCompatibilityLevel                                                           AS [MasterCompatibilityLevel]
	,suser_sname(tSD.[sid])                                                              AS [DatabaseOwner]
	,tSD.[crdate]                                                                        AS [DatabaseCreationDate]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'Collation'))              AS [DatabaseCollation]
	,@MasterCollation                                                                    AS [MasterCollation]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'Status'))                 AS [DatabaseStatus]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'Recovery'))               AS [DatabaseRecovery]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'Updateability'))          AS [DatabaseUpdateability]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'IsInStandBy'))            AS [DatabaseIsInStandBy]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'IsAutoClose'))            AS [DatabaseIsAutoClose]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'IsAutoShrink'))           AS [DatabaseIsAutoShrink]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'IsAutoCreateStatistics')) AS [DatabaseIsAutoCreateStatistics]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'IsAutoUpdateStatistics')) AS [DatabaseIsAutoUpdateStatistics]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'IsPublished'))            AS [DatabaseIsPublished]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'IsSubscribed'))           AS [DatabaseIsSubscribed]
	,NULL                                                                                AS [DatabaseIsEncrypted]
FROM
	[master].[dbo].[sysdatabases] tSD
ORDER BY
	tSD.[name]
;
