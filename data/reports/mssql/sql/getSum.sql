SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

SELECT
	 @@SERVERNAME AS [HostName]
	,1            AS [RecordSet]
	,GETDATE()    AS [CurrentTime]
	,'1 + 1'      AS [Exercise]
	,1 + 1        AS [Solution]
;

SELECT
	 @@SERVERNAME AS [HostName]
	,2            AS [RecordSet]
	,GETDATE()    AS [CurrentTime]
	,'2 + 2'      AS [Exercise]
	,2 + 2        AS [Solution]
;
