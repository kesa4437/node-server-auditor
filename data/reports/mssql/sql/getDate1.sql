SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

SELECT
	 @@SERVERNAME  AS [ServerName]
	,@@VERSION     AS [ServerVersion]
	,SUSER_NAME()  AS [SUserName]
	,USER_NAME()   AS [UserName]
	,GETDATE()     AS [EventTime]
	,DB_NAME()     AS [CurrentDatabase]
	,N'TestQuery1' AS [Message]
	,1             AS [RecordSet]
;

SELECT
	 @@SERVERNAME  AS [ServerName]
	,@@VERSION     AS [ServerVersion]
	,SUSER_NAME()  AS [SUserName]
	,USER_NAME()   AS [UserName]
	,GETDATE()     AS [EventTime]
	,DB_NAME()     AS [CurrentDatabase]
	,N'TestQuery2' AS [Message]
	,2             AS [RecordSet]
;

SELECT
	 @@SERVERNAME  AS [ServerName]
	,@@VERSION     AS [ServerVersion]
	,SUSER_NAME()  AS [SUserName]
	,USER_NAME()   AS [UserName]
	,GETDATE()     AS [EventTime]
	,DB_NAME()     AS [CurrentDatabase]
	,N'TestQuery3' AS [Message]
	,3             AS [RecordSet]
;
