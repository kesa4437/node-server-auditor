SELECT
	 'A'   AS `TypeDesc`
	,0     AS `IsMSShipped`
	,0     AS `IsIssue`
	,1     AS `TypeCount`
	,now() AS `CreateDateMin`
	,now() AS `CreateDateMax`

UNION ALL

SELECT
	 'B'   AS `TypeDesc`
	,0     AS `IsMSShipped`
	,0     AS `IsIssue`
	,2     AS `TypeCount`
	,now() AS `CreateDateMin`
	,now() AS `CreateDateMax`
;
