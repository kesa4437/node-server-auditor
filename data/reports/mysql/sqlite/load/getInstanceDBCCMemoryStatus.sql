SELECT
	 t.[MemoryManager] AS [MemoryManager]
	,t.[KB]            AS [KB]
FROM
	[${getInstanceDBCCMemoryStatus}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[MemoryManager] ASC
;
