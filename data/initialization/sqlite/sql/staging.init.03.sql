CREATE TABLE IF NOT EXISTS [dwh_InstanceDatabase]
(
	 [DateCreated]          [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[DateUpdated]          [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[EventDateTime]        [DATETIME] NOT NULL
	,[InstanceConnectionId] [INTEGER]  NOT NULL
	,[DatabaseId]           [TEXT]     NOT NULL
	,[DatabaseName]         [TEXT]     NULL
);
