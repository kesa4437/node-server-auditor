CREATE TABLE IF NOT EXISTS [dwh_InstanceDatabaseSize]
(
	 [DateCreated]           [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[DateUpdated]           [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[EventDateTime]         [DATETIME] NOT NULL
	,[InstanceConnectionId]  [INTEGER]  NOT NULL
	,[DatabaseId]            [TEXT]     NOT NULL
	,[DatabaseName]          [TEXT]     NULL
	,[DatabaseSizeMB]        [NUMERIC]  NULL
	,[DatabaseDataSizeMB]    [NUMERIC]  NULL
	,[DatabaseDataSizeUsedMB][NUMERIC]  NULL
	,[DatabaseLogSizeMB]     [NUMERIC]  NULL
	,[DatabaseLogSizeUsedMB] [NUMERIC]  NULL
);
