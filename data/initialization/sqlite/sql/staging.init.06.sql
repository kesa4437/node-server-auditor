CREATE INDEX IF NOT EXISTS [idx_dwh_InstanceSummaryProcesses] ON [dwh_InstanceSummaryProcesses]
(
	 [EventDateTime]
	,[InstanceConnectionId]
);
