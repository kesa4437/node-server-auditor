CREATE INDEX IF NOT EXISTS [idx_dwh_InstanceIOStatus] ON [dwh_InstanceIOStatus]
(
	 [EventDateTime]
	,[InstanceConnectionId]
	,[Disk]
	,[DatabaseId]
	,[DatabaseName]
);
