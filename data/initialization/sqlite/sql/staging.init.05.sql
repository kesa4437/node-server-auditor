CREATE TABLE IF NOT EXISTS [dwh_InstanceSummaryProcesses]
(
	 [DateCreated]                 [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[DateUpdated]                 [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[EventDateTime]               [DATETIME] NOT NULL
	,[InstanceConnectionId]        [INTEGER]  NOT NULL
	,[NumberOfSystemProcesses]     [INTEGER]  NULL
	,[NumberOfUserProcesses]       [INTEGER]  NULL
	,[NumberOfActiveUserProcesses] [INTEGER]  NULL
	,[NumberOfBlockedProcesses]    [INTEGER]  NULL
);
