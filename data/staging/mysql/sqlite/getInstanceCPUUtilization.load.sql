SELECT
	 t.[EventDateTime]                AS [EventDateTime]
	,t.[InstanceConnectionId]         AS [InstanceConnectionId]
	,t.[RecordTimeStamp]              AS [RecordTimeStamp]
	,t.[RecordRingBufferType]         AS [RecordRingBufferType]
	,t.[MemoryUtilization]            AS [MemoryUtilization]
	,t.[SystemIdle]                   AS [SystemIdle]
	,t.[SQLProcessCPUUtilization]     AS [SQLProcessCPUUtilization]
	,t.[OtherProcessesCPUUtilization] AS [OtherProcessesCPUUtilization]
FROM
	[dwh_InstanceCPUUtilization] t
WHERE
	t.[InstanceConnectionId] = $_id_connection
	AND t.[EventDateTime] > (
		SELECT
			DATETIME(MAX(t.[EventDateTime]), "-1 day")
		FROM
			[dwh_InstanceCPUUtilization] t
		WHERE
			t.[InstanceConnectionId] = $_id_connection
	)
ORDER BY
	t.[EventDateTime] DESC
;
