SELECT
	 t.[EventDateTime]               AS [EventDateTime]
	,t.[ServerName]                  AS [ServerName]
	,t.[NumberOfSystemProcesses]     AS [NumberOfSystemProcesses]
	,t.[NumberOfUserProcesses]       AS [NumberOfUserProcesses]
	,t.[NumberOfActiveUserProcesses] AS [NumberOfActiveUserProcesses]
	,t.[NumberOfBlockedProcesses]    AS [NumberOfBlockedProcesses]
FROM
	[${GetInstanceSummaryProcesses}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
	AND t.[EventDateTime] > (
		SELECT
			DATETIME(MAX(t.[EventDateTime]), "-1 day")
		FROM
			[${GetInstanceSummaryProcesses}{0}$] t
		WHERE
			t.[_id_connection] = $_id_connection
	)
ORDER BY
	 t.[EventDateTime] DESC
	,t.[ServerName] ASC
;
