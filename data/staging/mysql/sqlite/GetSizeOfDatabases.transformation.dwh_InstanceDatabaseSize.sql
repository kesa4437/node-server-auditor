INSERT INTO [dwh_InstanceDatabaseSize]
(
	 [EventDateTime]
	,[InstanceConnectionId]
	,[DatabaseId]
	,[DatabaseName]
	,[DatabaseSizeMB]
	,[DatabaseDataSizeMB]
	,[DatabaseDataSizeUsedMB]
	,[DatabaseLogSizeMB]
	,[DatabaseLogSizeUsedMB]
)
SELECT
	 t.[EventDateTime]
	,t.[_id_connection]
	,t.[DatabaseId]
	,t.[DatabaseName]
	,t.[DatabaseSizeMB]
	,t.[DatabaseDataSizeMB]
	,t.[DatabaseDataSizeUsedMB]
	,t.[DatabaseLogSizeMB]
	,t.[DatabaseLogSizeUsedMB]
FROM
	[${GetSizeOfDatabases}{0}$] t
	LEFT OUTER JOIN [dwh_InstanceDatabaseSize] dIDS ON
		dIDS.[EventDateTime] = t.[EventDateTime]
		AND dIDS.[InstanceConnectionId] = t.[_id_connection]
		AND dIDS.[DatabaseId] = t.[DatabaseId]
		AND dIDS.[DatabaseName] = t.[DatabaseName]
WHERE
	t.[_id_connection] = $_id_connection
	AND dIDS.[InstanceConnectionId] IS NULL
;
