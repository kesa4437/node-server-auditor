SELECT
	 t.[EventDateTime]      AS [EventDateTime]
	,t.[ServerName]         AS [ServerName]
	,SUM(t.[num_of_reads])  AS [num_of_reads]
	,SUM(t.[num_of_writes]) AS [num_of_writes]
FROM
	[${getInstanceIOStatus}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
	AND t.[EventDateTime] > (
		SELECT
			DATETIME(MAX(t.[EventDateTime]), "-1 day")
		FROM
			[${getInstanceIOStatus}{0}$] t
		WHERE
			t.[_id_connection] = $_id_connection
	)
GROUP BY
	 t.[EventDateTime]
	,t.[ServerName]
;
