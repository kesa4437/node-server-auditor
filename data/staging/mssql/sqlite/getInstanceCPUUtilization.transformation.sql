INSERT INTO [dwh_InstanceCPUUtilization]
(
	 [EventDateTime]
	,[InstanceConnectionId]
	,[RecordTimeStamp]
	,[RecordRingBufferType]
	,[MemoryUtilization]
	,[SystemIdle]
	,[SQLProcessCPUUtilization]
	,[OtherProcessesCPUUtilization]
)
SELECT
	 t.[EventDateTime]
	,t.[_id_connection]
	,t.[RecordTimeStamp]
	,t.[RecordRingBufferType]
	,t.[MemoryUtilization]
	,t.[SystemIdle]
	,t.[SQLProcessCPUUtilization]
	,t.[OtherProcessesCPUUtilization]
FROM
	[${getInstanceCPUUtilization}{0}$] t
	LEFT OUTER JOIN [dwh_InstanceCPUUtilization] dICU ON
		dICU.[EventDateTime] = t.[EventDateTime]
		AND dICU.[InstanceConnectionId] = t.[_id_connection]
WHERE
	t.[_id_connection] = $_id_connection
	AND dICU.[InstanceConnectionId] IS NULL
;
