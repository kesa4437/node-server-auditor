SELECT
	 strftime('%Y-%m-%d', t.[EventDateTime]) || ' 00:00:00.000' AS [EventDateTime]
	,t.[ServerName]                                             AS [ServerName]
	,SUM(t.[DatabaseDataSizeMB])                                AS [DatabaseDataSizeMB]
	,SUM(t.[DatabaseLogSizeMB])                                 AS [DatabaseLogSizeMB]
FROM
	[${GetSizeOfDatabases}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
	AND t.[EventDateTime] > (
		SELECT
			DATETIME(MAX(t.[EventDateTime]), "-7 day")
		FROM
			[${GetSizeOfDatabases}{0}$] t
		WHERE
			t.[_id_connection] = $_id_connection
	)
GROUP BY
	 strftime('%Y-%m-%d', t.[EventDateTime])
	,t.[ServerName]
;
